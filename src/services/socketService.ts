import { on } from 'node:events';
import io, {Socket} from 'socket.io-client';

const ENDPOINT = "localhost:3001";
export let socket: Socket;

export function initConnection():void{
    socket = io(ENDPOINT);
}

export function emit(connString: string, data: any, callback?:Function ):void{
    socket.emit(connString, data, callback);
}

export function finishConnection():void{
    socket.off();
}

export function receive(connString: string, callback:any): void{
    socket.on(connString, callback)     
}

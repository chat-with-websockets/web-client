import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Chat from './components/Chat/Chat';
import Join from './components/Join/Join';

function App() {
  return (
    <Router>
      <Route path="/chat" component={Chat}/>
      <Route path="/" exact component={Join}/>
    </Router>
  );
}

export default App;

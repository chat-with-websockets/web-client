import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import './styles.css';

export default function Join() {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');

    return (
        <div className="joinOuterContainer">
            <h3 className="app-title">Real Time Chat Application</h3>
            <div className="joinInnerContainer">
                <h1 className="heading">Join</h1>
                <div><input type="text" placeholder="Name" className="joinInput" onChange={e=>setName(e.target.value)}/></div>
                <div><input type="text" placeholder="Room" className="joinInput" onChange={e=> setRoom(e.target.value)}/></div>
                <Link to={`/chat?name=${name}&room=${room}`}>
                    <button disabled={!name || !room} className={'button mt-20'} type="button">Sign In</button>
                </Link>
            </div>
        </div>
    )
}

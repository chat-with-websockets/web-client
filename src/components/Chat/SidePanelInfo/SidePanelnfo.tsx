import React from 'react';
import onlineIcon from '../../../icons/onlineIcon.png';
import './styles.css';

export default function SidePanelnfo({ users }: any) {
    return (
        <div className="textContainer">
            <div>
                <h1>
                    Realtime Chat Application{' '}<span role="img" aria-label="emoji"> 💬 </span>
                </h1>
            </div>
            <div>
                <h2>People currently chatting:</h2>
                <div>
                    <h2>
                        {users.map((user: any) => (
                            <div key={user.id} className="activeItem">
                                {user.name}
                                <img alt="Online Icon" src={onlineIcon} className="online-icon" />
                            </div>
                        ))}
                    </h2>
                </div>
            </div>
        </div>
    );
}

import React from 'react';
import './styles.css';

export default function Input({ message, onInputChange, sendMessage }: any) {
    return (
        <form className="form">
            <input
                className="input"
                type="text"
                placeholder="Type a message"
                value={message}
                onChange={(e) => onInputChange(e)}
                onKeyPress={(e) => (e.key === 'Enter' ? sendMessage(e) : null)}
            />
            <button className="sendButton" onClick={e => sendMessage(e)}>Send</button>
        </form>
    );
}

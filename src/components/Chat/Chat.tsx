/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useRef } from 'react'
import queryString from 'query-string';
import './styles.css';
import InfoBar from './InfoBar/InfoBar';
import Input from './Input/Input';
import Messages from './Messages/Messages';
import SidePanelnfo from './SidePanelInfo/SidePanelnfo';
import * as socketService from '../../services/socketService';


interface IChatProps{
    location: any
}

interface IState{
    name: string,
    room: string,
    message: string,
    messages?: any[]
}

export default function Chat({location}:IChatProps) {
    const bottomRef = useRef() as any;    
    const [state, setState] = useState({
        name: '',
        room: '',
        message: ''
    } as IState);
    const [messages, setMessages]= useState([] as any[]);
    const [users, setUsers]=useState([] as any);

    useEffect(()=>{
        const {name, room} : any = queryString.parse(location.search);
        setState({...state, name, room});
        
        socketService.initConnection();
        socketService.emit('join', {name, room}, ()=>{ /*callback*/ });

        return ()=>{
            socketService.finishConnection();
        }
        
    }, [location.search])
    
    useEffect(()=>{
        socketService.receive('message', (message:any) => {
            setMessages([ ...messages, message ]);
            scrollToBottom();
        });            
        socketService.receive('roomData', (roomData:{room:any, users:any[]}) =>{
            setUsers(roomData.users);
        });        
    },[messages])

    const sendMessage = (event: React.KeyboardEvent<HTMLElement>) =>{
        event.preventDefault();

        if (state.message) 
            socketService.emit(
                'sendMessage', 
                state.message, 
                ()=>setState({...state, message:''})
            );        
    }
    
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {    
        setState({...state, message: event.target.value})
    };

    const scrollToBottom = () => {
        bottomRef.current.scrollIntoView({
            behavior: "smooth",
            block: "start",
        });
    };

    return (
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={state.room}/>                                
                <Messages messages={messages} name={state.name} bottomRef={bottomRef}/>
                <Input message={state.message} onInputChange={handleInputChange} sendMessage={sendMessage}/>
            </div>
            <SidePanelnfo users={users}/>
        </div>
    )
}

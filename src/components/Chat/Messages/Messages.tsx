import React from 'react';
import Message from '../Message/Message';
import './styles.css';

export default function Messages({ messages, name, bottomRef }: any) {
    return (
        <div className="messages">
            {messages.map((message: any, i: number) => (
                <div key={i}>
                    <Message message={message} name={name} />
                </div>
            ))}
            <div ref={bottomRef} className="list-bottom"></div>
        </div>
    );
}

import React from 'react'
import "./styles.css"

export default function Message({message, name}:any) {
    let isSendByCurrentUser = false;

    if ( message.user === name.trim().toLowerCase()) isSendByCurrentUser = true;

    return (
        isSendByCurrentUser 
        ? (
            <div className="messageContainer justifyEnd">
                <p className="sentText pr-10">{name}</p>
                <div className="messageBox backgroundBlue">
                    <p className="messageText colorWhite">{message.text}</p>
                </div>
            </div>
        )
        : (
            <div className="messageContainer justifyStart">
                <div className="messageBox backgroundLight">
                    <p className="messageText colorDark">{message.text}</p>
                </div>
                <p className="sentText pl-10">{message.user}</p>
            </div>
        )
    )
}
